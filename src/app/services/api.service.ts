import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


export enum SearchType {
  people = 'Personajes',
  planets = 'Planetas',
  films = 'Peliculas'
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url = 'https://swapi.co/api/';
  urlM = '';
  constructor(private http: HttpClient) { }

  getFilms(type: SearchType) {
    return this.http.get(`${this.url}${type}`);
  }
  
  getDetallMovie(id){
    return this.http.get(`${this.urlM}${id}`);
  }

}
