import { Component, OnInit } from '@angular/core';
import { ApiService, SearchType } from './../../services/api.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-moviedetails',
  templateUrl: './moviedetails.page.html',
  styleUrls: ['./moviedetails.page.scss'],
})
export class MoviedetailsPage implements OnInit {

  information = null;

  constructor(private activatedRoute: ActivatedRoute, private apiserv: ApiService) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    this.apiserv.getDetallMovie(id).subscribe(result => {
      this.information = result;
    });
  }

}
