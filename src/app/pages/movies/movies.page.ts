import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService, SearchType } from './../../services/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})
export class MoviesPage implements OnInit {

  films:Observable<any>;

  results:Observable<any>;
  type: SearchType = SearchType.films;
  constructor(private router: Router, private api: ApiService) { }

  ngOnInit() {
  }
  
  searchChanged() {
   this.films = this.api.getFilms(this.type);
  }

  openDetails(film) {
    let split = film.url.split('/');
    let filmId = split[split.length-2];
    this.router.navigateByUrl(`/tabs/films/${filmId}`);
  }
}
